#!/bin/bash
#aims at converting copied-pasted picrosses from nonograms.org into .txt files, suiting our picross builder, displayer and solver
#to use it : 
## copy the clues for the columns of a picross (take care of taking the whole rectangle)
## paste it into a file, let's call it columns_file
## do the same for the clues for the rows into rows_file
## call .\conversion_picross rows_number columns_number rows_file columns_file destination_file
#you can now call test("destination_file") in the Test class (Java part ofthis project)

conversion() {
    rows_file=$3
    rows=$1
    columns=$2
    destination_file=$5
    columns_file=$4
    echo $rows >> $destination_file
    echo $columns >> $destination_file
    `convert_rows "$rows" $rows_file $destination_file`
    `convert_columns "$columns" $columns_file $destination_file`
}

convert_columns() {
    file=$2
    destination_file=$3
    let "columns = $1 -1"
    i=0
    j=0
    k=0
    declare -A array
    old_IFS=$IFS  
    IFS=$'\n'  
    for row in $(cat $file)
    do 
        if [ $k -eq 0 ]
        then
            k=1
            row=`echo $row | tr -d "[:space:]"`
            array[$i,$j]=$row
            if [ $i -eq $columns ]
            then
                i=0
                let "j= $j + 1"
                k=0
            else 
                let "i = $i + 1"
            fi
        else
            k=0
        fi
    done
    let "rows = ${#array[*]} / $columns"
    IFS=$old_IFS
    for column in `seq 0 $columns`
    do 
        for row in `seq 0 $rows`
        do 
            array2[$row]=${array[$column,$row]}
        done
        `display_list $destination_file ${array2[*]}`
    done
}
convert_rows() {
    file=$2
    destination_file="$3"
    rows=$1
    echo $rows
    i=0
    k=0
    old_IFS=$IFS  
    IFS=$'\n' 
    let "columns = `sed -n '$=' $file` / ($rows * 2)"
    echo $columns
    for row in $(cat $file)
    do 
        if [ $k -eq 0 ]
        then
            k=1
            row=`echo $row | tr -d "[:space:]"`
            array[$i]=$row
            if [ $i -eq $columns ]
            then
                i=0
                echo ${array[*]}
                `display_list $destination_file ${array[*]}`
                k=0
            else 
                let "i = $i + 1"
            fi
        else
            k=0
        fi
    done
}
display_list() {
    file=$1
    shift
    row="["
    for i in $*
    do
        row="$row$i,"
    done  
    row="${row:0:${#row}-1}]"
    echo $row >> $file
}
    
conversion $*
