<?php 
include("connexion_database.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="robots" content="index, follow"/>
		<meta name="description" content="Site pour les accros à la lecture, créé par un lecteur acharné"/>
		<title> Lectur'Addicts</title>
		
		<script type="text/javascript">
		    var xhr = null;
            function creer_element_xhr() 
	            {
	            if (window.XMLHttpRequest || window.ActiveXObject) 
		            {
		            if (window.ActiveXObject) 
			            {
			            try 
				            {
				            xhr = new ActiveXObject("Msxml2.XMLHTTP");
				            } 
			            catch(e) 
				            {
				            xhr = new ActiveXObject("Microsoft.XMLHTTP");
				            }
			            } 
		            else 
			            {
			            xhr = new XMLHttpRequest();
			            }
		            }
	            else 
		            {
		            alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
		            return null;
		            };
	            return xhr;
	            };
            function envoi_donnees(pagetraitement,donnees,callback)
	            {
	            xhr=annule_requete_precedente(xhr);
	            xhr=null;
	            xhr=creer_element_xhr();
	            xhr.onreadystatechange=function()
		            {
		            if ((xhr.readyState==4)&&((xhr.status==0)||(xhr.status==200)))
			            {
			            callback(xhr.responseText);
			            }
		            };
	            xhr.open("POST",pagetraitement, true);
	            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	            xhr.send(donnees);
	            };
            function annule_requete_precedente()
	            {
	            if ((xhr!=null)&&(xhr.readyState!=0))
		            {
		            try
			            {
			            xhr.abort();
			            }
		            catch(err)
			            {
			            alert(err.message);
			            };
		            }
	            return xhr;
	            };
		    function enregistrer_tout() {
		        var liste = document.getElementsByClassName('iframe');
		        for(var k=0; k<liste.length; k++) {
		            console.log(liste[k]);
		            traiter_picross(liste[k]);
		        }
		        location.reload();
		    }
		    function traiter_picross(element) {
		        var picross = document.getElementById('nonogram_table');
		        var nom = element.id;
		        console.log(nom);
		        var columns = picross.getElementsByClassName('nmtt')[0];
		        var rows = picross.getElementsByClassName('nmtl')[0];
		        var clues_columns = new Array();
		        var liste = columns.getElementsByClassName('num');
		        /* on s'occupe des colonnes */
		        for(var i=0;i<liste.length;i++) {
		            var id = liste[i].id.replace("nmv","");
		            var t = id.split('_');
		            var color = liste[i].style.backgroundColor;
		            if(clues_columns[t[0]] == null) {
		                clues_columns[t[0]] = liste[i].getElementsByTagName('div')[0].innerHTML;
		            }
		            else {
	                    clues_columns[t[0]] += ','+clues_columns[t[0]]+liste[i].getElementsByTagName('div')[0].innerHTML;
		            }
	            }
	            //console.log(clues_columns);
	            var nb_columns = clues_columns.length;
	            console.log("nb_c : "+nb_columns);
	            var clues_rows = new Array();
	            liste = rows.getElementsByClassName('num');
		        for(var i=0;i<liste.length;i++) {
		            var id = liste[i].id.replace("nmh","");
		            var t = id.split('_');
		            var color = liste[i].style.backgroundColor;
		            if(clues_rows[t[1]] == null) {
		                clues_rows[t[1]] = liste[i].getElementsByTagName('div')[0].innerHTML;
		            }
		            else {
	                    clues_rows[t[1]] += ','+liste[i].getElementsByTagName('div')[0].innerHTML;
	                }
	            }
	            //console.log(clues_rows)
	            var nb_rows = clues_rows.length;
	            console.log("nb_r : "+nb_rows);
		        var s = changeToString(clues_rows);
		        s+=changeToString(clues_columns);
		        s = ""+(nb_rows)+'\n'+(nb_columns)+'\n'+s;
		        console.log(s);
		        var donnees=encodeURIComponent(s);
		        nom=encodeURIComponent(nom);
	            donnees='s='+donnees+'&n=BW/'+nom;
	            envoi_donnees("enregistrer_picross.php", donnees, function(a){console.log('ok');}); 
		    }
		    function traiter_item(item, classe, a) {
	            return a;
		    }
		    function changeToString(a) {
		        var s = "";
		        for(var k = 0; k<a.length;k++) {
		            s = s +'['+ a[k]+']' +'\n';
		        }
		        return s;
		    }
		</script>
	</head>
	
	<body onload="enregistrer_tout();">
<?php

ini_set('display_errors', TRUE);
error_reporting(E_ALL);
	echo 'OK';
	echo isset($bdd);
	$req=$bdd->query('SELECT * FROM url_picross_bw WHERE V=0 LIMIT 0,1');
	while($donnees=$req->fetch())
		{
		/*retire l'url des urls à consulter*/
		$req2=$bdd->prepare('UPDATE url_picross_bw SET V=1 WHERE ID=:ID');
		$req2->execute(array('ID'=>$donnees['ID']));
		$req2->closeCursor();
		/* télécharge les données voulues */
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $donnees['L']);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		//curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
		$resultat = curl_exec($ch);
		curl_close($ch);
		if($resultat!='')
		    {
			libxml_use_internal_errors(true);
			$page=new DOMDocument();
			$page->loadHTML($resultat);
			$nom = str_replace(" ","_",str_replace("»","",explode("«",$page->getElementsByTagName('h1')->item(0)->getElementsByTagName('span')->item(0)->textContent)[1]));
			echo '<div class="iframe" id="'.$nom.'">'.$resultat.'</div>';
			}
			
			
			/*foreach($liste as $donnees2)
				{
				/* début des données à enregistrer */
				/* image */
				/*$img=$donnees2->getElementByTagName('img')[0];
				$img=$img->getAttribute("src");
				/* fin de l'image */
				/*$nom=$donnees2->getElementsByClass('nameStyle')[0]->innerText;
				$prix=$donnees2->getElementsByClass('product-sales-price')[0]->innerText;
				/* fin des données à enregistrer */
		
				/*enregistre la photo*/
				/*$img2=$nom.'.jpg';
				$fp = fopen ('../../uploads/'.$img2, 'w+');              // open file handle
				$ch = curl_init($img);
				// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // enable if you want
				curl_setopt($ch, CURLOPT_FILE, $fp);          // output to file
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10000000);      // some large value to allow curl to run for a long time
				curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
				// curl_setopt($ch, CURLOPT_VERBOSE, true);   // Enable this line to see debug prints
				curl_exec($ch);
				curl_close($ch);                              // closing curl handle
				fclose($fp);
				/* fin du traitement de la photo */
				/*if($donnees['U'][0]=='f')
					{
					$c="F";
					}
				elseif ($donnees['U'][1]='k')
					{
					$c="UK";
					}
				else
					{
					$c="US";
					}
				/* envoi des données */
				/*enregistrer_donnees_maje(array($nom,$prix),$bdd,$c);
				/* fin de l'envoi des données */
				//}
			//}
		};?>
		
		</body>
</html>
