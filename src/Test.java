import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
  class to launch the picross and the solver
  @author guillaume
 
 */
public class Test{
	public static void main(String[] args) {
		try{
			/* read in the specified file */
			InputStream ips=new FileInputStream("bw.txt"); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String ligne;
			int i = 0;
			while(((ligne=br.readLine())!=null)&&(i<100)) {
				i++;
				System.out.println(ligne);
				test("BW/"+ligne+".txt");
			}
			br.close();
			/* read in the specified file */
			i = 0;
			String ligne2;
			InputStream ips2=new FileInputStream("c.txt"); 
			InputStreamReader ipsr2=new InputStreamReader(ips2);
			BufferedReader br2=new BufferedReader(ipsr2);
			while(((ligne2=br2.readLine())!=null)&&(i<60)) {
				i++;
				
					testColored("C/"+ligne2+".txt");
					/*String file = "C/Aquarius.txt";
					PicrossColored p = new PicrossColored(new File(file).getAbsolutePath());
					Window f = new Window(p, file);
					p.solveOpt();
					System.out.println("test : "+p.testColumn(0, 2, -2));
			*/}
			br2.close();
		}
		catch(IOException e) {System.out.println(e.toString());}
	}
	
	/**
	  create a picross, solve it and display it
	  @param file
	  @author Guillaume
	 */
	public static void test(String file) {
		Picross p = new Picross(new File(file).getAbsolutePath());
		Window f = new Window(p, file);
		p.solveOpt();
		//try {Thread.sleep(1000);}catch(Exception e){}
	}
	
	/**
	  create a picrossColored, solve it and display it
	  @param file
	  @author Guillaume
	 */
	public static void testColored(String file) {
		PicrossColored p = new PicrossColored(new File(file).getAbsolutePath());
		Window f = new Window(p, file);
		p.solveOpt();
		//try {Thread.sleep(1000);}catch(Exception e){}
	}
}
