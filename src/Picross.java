import javax.swing.JPanel;

import java.awt.*;
import java.io.*;
import java.util.*;

public class Picross extends JPanel {
    /*
     * definition of the dimensions of each cell
     */
	int cell_width;
    int cell_height;
    /*
     * how many cells do we have ?
     */
    int nb_columns;
    int nb_rows;
    int nb_clues_rows;
    int nb_clues_columns;
    /* 
     * what colors will we use ?
     */
    Color unknown;
    Color checked;
    Color unchecked;
    /*
     * what clues do we have ?
     */
    LinkedList<LinkedList<Integer>> clues_columns;
    LinkedList<LinkedList<Integer>> clues_rows;
    LinkedList<boolean[]>[] possibilitiesRows;
    LinkedList<boolean[]>[] possibilitiesColumns;
    /*
     * values of the grid
     */
    Boolean[][] a;//a[column][row]
    Cell[][] c;
    
    /**
     * (non-Javadoc)
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    public void paintComponent(Graphics g){
    	super.paintComponent(g);
    }
    
    /**
     * compute how many rows on top, and columns on left, we have to add for the clues
     * @param l : list of the clues
     * @return
     */
    public int maxclues(LinkedList<LinkedList<Integer>> l) {
    	int m = 0;
    	for(LinkedList<Integer> l1:l){
    		m = Math.max(m, l1.size());
    	}
    	return m;
    }
    
    /**
     * deal with the input file
     * affect nb_rows, nb_columns, clues_rows, clues_columns
     * @param file : the name of the file were with the clues for the picross
     */
    public void fileTreatment(String file) {
		try{
			/* read in the specified file */
			InputStream ips=new FileInputStream(file); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String ligne;
			/* save the read data in the picross */
			this.nb_rows = Integer.parseInt(br.readLine());//first line = number of cells in height
			this.nb_columns = Integer.parseInt(br.readLine());//second line = number of cells in width
			this.clues_rows = new LinkedList<LinkedList<Integer>>();
			this.clues_columns = new LinkedList<LinkedList<Integer>>();
			
			//computes clues_rows
			for(int k = 0 ; k<nb_rows; k++){//for every row
				LinkedList<Integer> l = new LinkedList<Integer>();
				ligne = br.readLine();//get the associated clues' line
				ligne = ligne.substring(1, ligne.length()-1);//get rid of the []
				if(ligne.length() > 0) {
					String[] a = ligne.split(",");//dissociate the clues
					for(int i = 0; i<a.length;i++){
						l.add(Integer.parseInt(a[i]));//fill the List
					}
				}
				this.clues_rows.add(l);
			}
			//computes clues_columns, same as previously
			for(int k = 0 ; k<nb_columns; k++){
				LinkedList<Integer> l = new LinkedList<Integer>();
				ligne = br.readLine();
				ligne = ligne.replace("[", "");
				ligne = ligne.replace("]","");
				if(ligne.length() > 0) {
					String[] a = ligne.split(",");
					for(int i = 0; i<a.length;i++){
						l.add(Integer.parseInt(a[i]));
					}
				}
				this.clues_columns.add(l);
			}
			br.close(); //end of reading file
			System.out.println("file read");
		}
		catch (Exception e){
			System.out.println(e.toString());
		}
    	
    }
    
    /**
     * create a shallow copy of a list of list of integer
     * @param l
     * @return l1, shallow copy of l
     */
    public LinkedList<LinkedList<Integer>> copyList(LinkedList<LinkedList<Integer>> l) {
    	LinkedList<LinkedList<Integer>> l1 = new LinkedList<LinkedList<Integer>>();
        for(LinkedList<Integer> s:l){
        	LinkedList<Integer> l2 = (LinkedList<Integer>) s.clone();
        	l1.add(l2);
        }
        return l1;
    }
    

    /**
     * generates the graphic aspect of our picross
     */
    public void generateGraphics() {
    	/* copy the clues in order to avoid destroying them */
    	LinkedList<LinkedList<Integer>> clues_column = copyList(clues_columns);
    	LinkedList<LinkedList<Integer>> clues_row = copyList(clues_rows);
		/* we use a grid layout */
        setLayout(new GridBagLayout());
    	/* object to locate components */
        GridBagConstraints gbc = new GridBagConstraints();
        /* no cell is merged */
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        Integer value;//this will be the eventual value of the clue
        
        /* create the display for the columns clues */
        for(int k = nb_clues_columns-1 ; k >= 0 ; k--){
            for(int i = 0 ; i < nb_columns ; i++){//for each column
				if(!clues_column.get(i).isEmpty()){
					value = clues_column.get(i).removeLast();
				}
				else {
					value = null;
				}
                Cell c = new Cell(cell_width, cell_height, unchecked, value);
                //set the cell at (nb_clues_rows+column number, nb_clues_columns-number_clue)
                gbc.gridx = nb_clues_rows + i;
                gbc.gridy = k;
                if(i == nb_columns){//go to the next line
                	gbc.gridwidth = GridBagConstraints.REMAINDER;
                }
                add(c,gbc);
            }
        }
        
        /* create the display for the rows clues */
        for(int i = 0 ; i < nb_rows ; i++){
            for(int k = nb_clues_rows-1 ; k >= 0 ; k--){
				if(!clues_row.get(i).isEmpty()){
					value = clues_row.get(i).removeLast();
				}
				else {
					value = null;
				}
                Cell c = new Cell(cell_width, cell_height, unchecked, value);
                gbc.gridx = k;
                gbc.gridy = nb_clues_columns+i;
                add(c,gbc);
            }
        }
        /* sets the inside of the picross */
        for(int k = 0 ; k < nb_columns ; k++){
            for(int i = 0 ; i < nb_rows ; i++){
                Cell c = new Cell(cell_width, cell_height, unknown, null);
                gbc.gridx = nb_clues_rows+k;
                gbc.gridy = nb_clues_columns+i;
                if(k == nb_columns-1){
                	gbc.gridwidth = GridBagConstraints.REMAINDER;
                }
                this.c[k][i] = c;
                a[k][i] = null;
                add(c,gbc);
            }
        }
        this.setPreferredSize(new Dimension(600, 600));
        this.setMinimumSize(new Dimension(600, 600));
    }
    
    /**
     * build a picross from a file of clues
     * @param file : the name of the file with all the indications to build the picross
     */
    public Picross(String file) {
		/* Instantiate the constants in the picross */
    	fileTreatment(file);
    	this.cell_width = 50;
	    this.cell_height = 50;
	    this.unknown = Color.GRAY;
	    this.checked = Color.BLACK;
	    this.unchecked = Color.WHITE;
	    this.a = new Boolean[nb_columns][nb_rows];
	    this.c = new Cell[nb_columns][nb_rows];
	    /* number of additional cells for the clues */
	    this.nb_clues_columns = maxclues(clues_columns); 
	    this.nb_clues_rows = maxclues(clues_rows);
	    this.possibilitiesRows = new LinkedList[this.nb_rows];
	    this.possibilitiesColumns = new LinkedList[this.nb_columns];
	    this.setSize(new Dimension(600, 600));
	    generateGraphics();
	    
	    /* compute all possibilities for rows and columns */
	    for(int k = 0; k<this.nb_rows; k++) {
	    	this.possibilitiesRows[k] = transformPossibilities(
	    			computePossibilities(k, (LinkedList<Integer>) this.clues_rows.get(k).clone(), 0, true),
	    			true, k);
	    }
	    for(int k = 0; k<this.nb_columns; k++) {
	    	this.possibilitiesColumns[k] = transformPossibilities(
	    			computePossibilities(k, (LinkedList<Integer>) this.clues_columns.get(k).clone(), 0, false), 
	    			false, k);
	    }
	    System.out.println("Picross instancié");
		//this.setPreferredSize(new Dimension(cell_width*(nb_columns + nb_clues_rows),cell_height*(nb_rows)));
    }
    
    /**
     * tests if the disposition on the column is convenient with the column's clues until row r
     * @param c : int, index of the tested column
     * @param r : int, index of the first unconsidered row
     * @return
     */
    public boolean testColumn(int c, int r) {
    	int i = 0;
    	int j = 0;
    	LinkedList<Integer> l = (LinkedList<Integer>) clues_columns.get(c).clone();//clone the clues for not destroying them
    	while(i<r){//while we still match the given area
    		if(a[c][i]){
    			//if the cell is checked
    			j++;//we have one more consecutive checked cell 
    			if(l.isEmpty() || j>l.getFirst()) {
        			//if we have more consecutive checked cells than expected
    				return false;
    			}
    		}
    		if(!a[c][i] && j != 0){
    			//if the cell isn't checked, and we have begun a new checked cells' block
    			if(j<l.pop() && i<r-1){//if we have less consecutive checked cells than expected
    				return false;
    			}
    			//we have enough consecutive cells
    			j = 0;//show must go on, but we change clue
    		}
    		i++;
    	}
    	return r<nb_rows || l.isEmpty() || (j == l.pop() && l.isEmpty() && r == nb_rows);
    }

    /**
     * fill the row with unchecked cells
     * @param r index of row
     * @param i index of beginning of unknown
     */
    public void endRow(int r, int i) {
    	for(int k=i;k<nb_columns;k++) {
    		a[k][r] = false;
    		c[k][r].changeColor(unchecked);
    	}
    }
    
    /**
     * tests if every column is convenient with the clues until column c
     * @param c : int, index of the first unconsidered column
     * @param r : int, index of the first unconsidered row
     * @return if the disposition suits to the clues
     */
    public boolean testColumns(int c, int r) {
    	boolean b1 = true;
    	int i = 0;
    	while(b1 && i<c) {
    		b1 &= testColumn(i,r);
    		i++;
    	}
    	return b1;
    }
    
    /**
     * compute all the possibilities for an item (row or column) of our picross
     * @param r : int, index of our item
     * @param clues : list of the considered clues for the item
     * @param j : index of first placement of the first clue
     * @param b : if the item is a row or not
     * @return the list of possibilities in following format : array containing the indexes of the first cell of each block
     */
    public LinkedList<int[]> computePossibilities(int r, LinkedList<Integer> clues, int j, boolean b) {
    	//for each possibility to place the first block following the clues
    	int nb;
    	if(b) {
    		nb = this.nb_columns;
    	}
    	else {
    		nb = this.nb_rows;
    	}
    	if(clues.isEmpty())   {//if we don't have a clue at all
    		LinkedList<int[]> l = new LinkedList<int[]>();
    		int[] t = new int[0];
    		l.add(t);
    		return l;
    	}
    	else {
			LinkedList<int[]> l = new LinkedList<int[]>();
			int n = clues.pop();
			for(int k = j; k<nb; k++) {//for each possibility of begin cell
				LinkedList<int[]> l1;
		    	if(clues.isEmpty()) {//if this clue was the last
		    		if(k+n<=nb){
		    			int[] t = {k};
		    			l.add(t);
		    		}
		    	}
		    	else {
	    			l1 = computePossibilities(r, (LinkedList<Integer>) clues.clone(), k+n+1, b);
	    			if(!l1.isEmpty()) {
	        			for(int[] t: l1){
	        				int[] t1 = new int[t.length+1];
	        				t1[0] = k;
	        				for(int j1 = 1;j1<=t.length; j1++){
	        					t1[j1] = t[j1-1];
	        				}
	        				l.add(t1);
	        			}
	    			}
		    	}
		    	//if(k==0)System.out.println(r+" possibilities computed");//allows us to check where the programm ran out of memory (just in case)
			}
			return l;
    	}
	}

    public void computePossibilitiesOptimised(int r, LinkedList<Integer> clues, boolean b) {
    	int nb;
    	if(b) {
    		nb = this.nb_columns;
    	}
    	else {
    		nb = this.nb_rows;
    	}
    	/* l is the list of the last possible index of each block */
    	LinkedList<Integer> l = new LinkedList<Integer>();
    	int i = nb-1;
    	for(int k=clues.size()-1; k>=0; k--) {
    		i -= clues.get(k);
    		if(k != clues.size()-1) {
    			i--;
    		}
    		l.addLast(i);
    	}
    	/*
    	 * compute possibilities with that reduced intervals
    	 */
    }
    
    /**
     * transform the list of possibilities computed recursively (with only the beginning of each block) into 
     * the list of possibilities usable in the combination and exclusion part (with the value of each cell)
     * @param l = list of computed possibilities
     * @param b = row (true) or column
     * @param i = index of our item
     * @return
     */
    public LinkedList<boolean[]> transformPossibilities(LinkedList<int[]> l, boolean b, int i) {
		LinkedList<boolean[]> l1 = new LinkedList<boolean[]>();
		LinkedList<LinkedList<Integer>> clues;
		int nb;
    	if(b){//we are dealing with rows
    		nb = this.nb_columns;
    		clues = this.clues_rows;
    	}
    	else {//we are dealing with columns
    		nb = this.nb_rows;
    		clues = this.clues_columns;
    	}
    	while (!l.isEmpty()) {
        	boolean[] t = new boolean[nb];
    		int[] t1 = l.pop();
    		for(int k=0; k<nb; k++) {
    			t[k] = false;
    		}
    		for(int k=0; k<t1.length;k++) {//for each clue
    			int n = clues.get(i).get(k)+t1[k];
    			for(int j=t1[k];j<n; j++) {//for each element of the block
    				t[j] = true;
    			}
    		}
    		l1.add(t);
    	}
    	return l1;
    }
    
   /**
     * places a block of length l on the row r after index i, returns index of last occupied cell
     * @param l : length of the block
     * @param i : index of the the first cell of the block
     * @return if the block can be placed
     */
    public boolean placeBlock(int l, int i){
    	return i+l<= nb_columns;
    }
    
    /**
     * place a computed "int" possibility on a row
     * @param r : index of the row
     * @param po : considered possibility
     * @param clues : list of the clues for the row
     */
    public void placeIntPossibilityRow(int r, int[] po, LinkedList<Integer> clues) {
    	endRow(r, 0);
    	for(int k = 0; k<po.length; k++) {//for each block in that line
    		for(int i = 0; i<clues.get(k);i++){
           		a[po[k]+i][r] = true;
           		c[po[k]+i][r].changeColor(checked);
    		}
    	}
    }
    
    /**
     * place a transformed (boolean) possibility on a row
     * @param r : index of the row
     * @param po : considered possibility
     */
    public void placePossibilityRow(int r, boolean[] po) {
    	for(int k = 0; k<this.nb_columns; k++) {//for each cell in that line
          	a[k][r] = po[k];
          	if(po[k]){
          		c[k][r].changeColor(checked);
          	}
          	else {
          		c[k][r].changeColor(unchecked);
          	}
    	}
    }
    
    /**
     * place an allowed possibility on the row
     * @param r : index of the row
     * @param po : list of possibilities for that row
     * @return if such a possibility exists
     */
    public boolean placeAllowedPossibilityRow(int r, LinkedList<boolean[]> po) {
    	if(po.isEmpty()){
    		return false;
    	}
    	else {
        	boolean[] p = po.pop();
        	placePossibilityRow(r, p);
        	return testColumns(nb_columns, r+1) || placeAllowedPossibilityRow(r, po);
    	}
    }

    /**
     * solves the backtracking for a row
     * @param r : index of the first unconsidered row
     * @param as : current state of the possibilities
     * @return if there is a solution to the problem
     */
    public boolean solveBacktrackingRows(int r, LinkedList<boolean[]>[] as) {
    	if(r == this.nb_rows) {
    		return true;
    	}
    	else {
    		if(placeAllowedPossibilityRow(r, as[r])){
    			//if we are able to place one of the possibilities
    			return solveBacktrackingRows(r+1, as);
    		}
    		else {
    			as[r] = (LinkedList<boolean[]>) this.possibilitiesRows[r].clone();
    			for(int k=0; k<this.nb_columns;k++) {
    				a[k][r] = null;
    				c[k][r].changeColor(unknown);
    			}
    			return solveBacktrackingRows(r-1, as);
    		}
    	}
    }
    
    /**
     * solve the picross using backtracking
     */
    public void solveBacktracking() {
    	LinkedList<boolean[]>[] as = new LinkedList[this.nb_rows];
    	for(int k=0; k<this.nb_rows; k++) {
    		as[k] = (LinkedList<boolean[]>) possibilitiesRows[k].clone();
    	}
    	System.out.println(this.solveBacktrackingRows(0, as));
    }
    
    /**
     * compare an array of Boolean to an array of boolean, and update the first one
     * @param a : state of a row
     * @param p : possibility
     * @return if there is a modification brought by this possibility
     * @author guillaume, Vincent
     */
    public boolean compareItem(Boolean[] a, boolean[] p) {
    	boolean b = false;
    	for(int k=0; k<a.length; k++) {
    		if(a[k] != null) {
    			if(a[k] != p[k]) {
    				a[k] = null;
    				b = true;
    			}
    		}
    	}
    	return b;
    }
    
    /**
     * 
     * @param possibilities
     * @param nb
     * @param b
     * @author guillaume
     */
    public void initializeItem(LinkedList<boolean[]>[] possibilities, int nb, boolean b) {
    	if(b) {
    		possibilities = this.possibilitiesRows;
    		nb = nb_columns;
    	}
    	else {
    		possibilities = this.possibilitiesColumns;
    		nb = nb_rows;
    	}
    }
    
    /**
     * modify a cell according to the type of considered item (row or column)
     * @param b : if the item is a row
     * @param v : the value to give to the cell
     * @param i : first index
     * @param j : second index
     * @param c : Color to give to the cell
     * @author guillaume, Vincent
     */
    public void modifyFollowingItem(boolean b, boolean v, int i, int j, Color c) {
		if(b) {
			this.a[j][i] = v;
			this.c[j][i].changeColor(c);
		}
		else {
			this.a[i][j] = v;
			this.c[i][j].changeColor(c);
		}
    }
    
    /**
     * computes which cells we can tell they are black, and update the state and color of the picross, for an item (row or column)
     * @param b : if the item is a row
     * @param i : index of the item
     * @param possibilities : list of possibilities for the item
     * @param nb : amount of opposite items
     * @author guillaume, Vincent
     */
    public void interpolatePossibilities(boolean b, int i, LinkedList<boolean[]> possibilities, int nb) {
		Boolean[] u = new Boolean[nb];
		/* u will be the first possibility */
		for(int j = 0; j<nb;j++) {
			u[j] = possibilities.getFirst()[j];
		}
		/* for each possibility, we compare item to the to-date state of the picross item */
		for(boolean[] v:possibilities) {
			this.compareItem(u, v);
    	}
		/* update a and c */
    	for(int j = 0; j<nb;j++){
    		if(u[j] != null) {
    			if(u[j]) {
    				this.modifyFollowingItem(b, u[j], i, j, this.checked);
    			}
    			else {
    				this.modifyFollowingItem(b, u[j], i, j, this.unchecked);
    			}
    		}
    	}
    }
    
    /**
     * launched the combination algorithm on all items (rows or columns)
     * @param b : if the items are rows 
     * @author Vincent
     */
	public void interpolation(boolean b) {
    	LinkedList<boolean[]>[] possibilities;
    	int nb = 0;
    	if(b) {
    		possibilities = this.possibilitiesRows;
    		nb = nb_columns;
    	}
    	else {
    		possibilities = this.possibilitiesColumns;
    		nb = nb_rows;
    	}
    	for(int i=0; i<possibilities.length; i++) {
    		/* apply the interpolation algorithm on each row or column */
    		this.interpolatePossibilities(b, i, possibilities[i], nb);
    	}
    }
    
    /**
     * eliminate possibilities that doesn't match the current state of the items
     * @param b : if the items are rows
     * @author Vincent
     */
    public boolean elimination(boolean b){
    	boolean b1 = false;
    	LinkedList<boolean[]>[] possibilities = null;
    	int nb = 0;
    	if(b) {
    		possibilities = this.possibilitiesRows;
    		nb = nb_columns;
    	}
    	else {
    		possibilities = this.possibilitiesColumns;
    		nb = nb_rows;
    	}
    	for(int i=0; i<possibilities.length; i++) {
    		LinkedList<boolean[]> l = possibilities[i];
    		LinkedList<boolean[]> l2 = new LinkedList<boolean[]>();
    		for(boolean[] v:l) {//for each possibility
    			int c = 0;
    			for(int j = 0; j<nb; j++) {
    				Boolean u;
    				if(b) {
    					u = a[j][i];
    				}
    				else {
    					u =  a[i][j];
    				}
    				if(u != null) {//if the cell is already determined
    					if (!v[j] == u) {//if there is a difference between the possibility and the state of the picross
    						c = c+1;
    						break;
    					}
    				}
    			}
    			if(c == 0) {//there is no difference, the possibility is still a possibility, keep consider it
					l2.add(v);
    			}
    			else {
    				b1 = true;
    			}
    		}
    		possibilities[i] = l2;//we keep only the possibilities that match the current state of the grid
    	}
    	return b1;
    }
    
    /**
     * solve the picross using combination and elimination
     * @author Vincent
     */
    public void solveOpt(){
    	boolean b = true;
    	while(b){
        	this.interpolation(true);
        	this.interpolation(false);
    		b = this.elimination(true) || this.elimination(false);
    	}
    	this.solveBacktracking();
    }
}
