import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Test;


public class PicrossTest {
	Picross p = new Picross("BW/test2.txt");
	Picross p1 = new Picross("BW/test2.txt");
	PicrossColored p2 = new PicrossColored("C/Tiny_fish.txt");
	PicrossColored p3 = new PicrossColored("C/Tiny_deer.txt");
	
	public static boolean equalArrays(int[] i, int[] j) {
		if(i.length != j.length) {
			return false;
		}
		else {
			boolean b = true;
			for(int k = 0; k<i.length; k++) {
				b&=(i[k]==j[k]);
			}
			return b;
		}
	}
	
	public static boolean equalListArray(LinkedList<int[]>i, LinkedList<int[]>j) {
		if(i.size() != j.size()) {
			return false;
		}
		else {
			boolean b = true;
			for(int k = 0; k<i.size(); k++) {
				b&=equalArrays(i.get(k), j.get(k));
			}
			return b;
		}
	}
	
	@Test
	public void testComputePossibilities() {
		LinkedList<int[]> po = p.computePossibilities(0, p.clues_rows.get(0), 0, true);
		LinkedList<int[]> po1 = new LinkedList<int[]>();
		int[] t = {0,2};
		po1.add(t);
		int[] t1 = {0,3};
		po1.add(t1);
		int[] t11 = {1,3};
		po1.add(t11);
		assertTrue(equalListArray(po, po1));
	}
	
	@Test
	public void testTestColumns() {
		p.a[0][0] = false;
		p.a[1][0] = true;
		p.a[2][0] = false;
		p.a[3][0] = true;
		p.a[0][1] = false;
		p.a[1][1] = false;
		p.a[2][1] = true;
		p.a[3][1] = false;
		assertTrue(p.testColumns(p.nb_columns, 2));
	}
	
	@Test
	public void testTestColumns2() {
		p.a[0][0] = false;
		p.a[1][0] = true;
		p.a[2][0] = false;
		p.a[3][0] = true;
		assertTrue(p.testColumns(p.nb_columns, 1));
	}
	
	@Test
	public void testPlaceIntPossibilityRow() {
		int [] t = {1,3};
		LinkedList<Integer> l = new LinkedList<Integer>();
		l.add(1);
		l.add(1);
		p.placeIntPossibilityRow(0, t, l);
		Boolean[][] a = new Boolean[p.nb_columns][p.nb_rows];
		a[0][0] = false;
		a[1][0] = true;
		a[2][0] = false;
		a[3][0] = true;
		assertEquals(p.a, a);
	}
	
	@Test
	public void testTestColumnColored() {
		p2.a[0][0] = -2;
		p2.a[1][0] = -2;
		p2.a[2][0] = -2;
		p2.a[3][0] = -2;
		p2.a[4][0] = 0;
		p2.a[0][1] = 0;
		p2.a[1][1] = -2;
		p2.a[2][1] = -2;
		p2.a[3][1] = 0;
		p2.a[4][1] = 0;
		assertTrue(p2.testColumn(2, 2, -2));
	}
	
	@Test
	public void testTestColumnsColored() {
		p2.a[0][0] = -2;
		p2.a[1][0] = -2;
		p2.a[2][0] = -2;
		p2.a[3][0] = 0;
		p2.a[4][0] = -2;
		p2.a[0][1] = 0;
		p2.a[1][1] = -2;
		p2.a[2][1] = -2;
		p2.a[3][1] = 0;
		p2.a[4][1] = 0;
		p2.a[0][2] = 0;
		p2.a[1][2] = 2;
		p2.a[2][2] = 2;
		p2.a[3][2] = -2;
		p2.a[4][2] = -2;
		assertTrue(p2.testColumns(p2.nb_columns, 3));
	}
	
	@Test
	public void testTestColumnColored2() {
		p2.a[0][0] = -2;
		p2.a[1][0] = -2;
		p2.a[2][0] = -2;
		p2.a[3][0] = 0;
		p2.a[4][0] = -2;
		p2.a[0][1] = 0;
		p2.a[1][1] = -2;
		p2.a[2][1] = -2;
		p2.a[3][1] = 0;
		p2.a[4][1] = 0;
		p2.a[0][2] = 0;
		p2.a[1][2] = 2;
		p2.a[2][2] = 2;
		p2.a[3][2] = -2;
		p2.a[4][2] = -2;
		assertTrue(p2.testColumn(3, 3, -2));
	}
	
	@Test
	public void testTestColumnColored3() {
		p2.a[0][0] = 1;
		p2.a[1][0] = -2;
		p2.a[2][0] = -2;
		p2.a[3][0] = 0;
		p2.a[4][0] = -2;
		p2.a[0][1] = 0;
		p2.a[1][1] = -2;
		p2.a[2][1] = -2;
		p2.a[3][1] = 0;
		p2.a[4][1] = 0;
		p2.a[0][2] = 0;
		p2.a[1][2] = 2;
		p2.a[2][2] = 2;
		p2.a[3][2] = -2;
		p2.a[4][2] = -2;
		assertFalse(p2.testColumn(0, 3, -2));
	}
		
	@Test
	public void testTestColumnsColored4() {
		p3.a[0][0] = -2;
		p3.a[1][0] = 1;
		p3.a[2][0] = -2;
		p3.a[3][0] = 1;
		p3.a[4][0] = -2;
		p3.a[5][0] = -2;
		p3.a[0][1] = -2;
		p3.a[1][1] = -2;
		p3.a[2][1] = 1;
		p3.a[3][1] = 0;
		p3.a[4][1] = 2;
		p3.a[5][1] = -2;
		p3.a[0][2] = -2;
		p3.a[1][2] = -2;
		p3.a[2][2] = -2;
		p3.a[3][2] = 0;
		p3.a[4][2] = 0;
		p3.a[5][2] = 0;
		p3.a[0][3] = 0;
		p3.a[1][3] = -2;
		p3.a[2][3] = -2;
		p3.a[3][3] = 0;
		p3.a[4][3] = -2;
		p3.a[5][3] = -2;
		p3.a[0][4] = -2;
		p3.a[1][4] = 0;
		p3.a[2][4] = 0;
		p3.a[3][4] = 0;
		p3.a[4][4] = -2;
		p3.a[5][4] = -2;
		p3.a[0][5] = -2;
		p3.a[1][5] = 0;
		p3.a[2][5] = 0;
		p3.a[3][5] = 0;
		p3.a[4][5] = -2;
		p3.a[5][5] = -2;
		p3.a[0][6] = 0;
		p3.a[1][6] = -2;
		p3.a[2][6] = -2;
		p3.a[3][6] = 0;
		p3.a[4][6] = -2;
		p3.a[5][6] = -2;
		p3.a[0][7] = 0;
		p3.a[1][7] = -2;
		p3.a[2][7] = -2;
		p3.a[3][7] = 0;
		p3.a[4][7] = -2;
		p3.a[5][7] = -2;
		
		assertTrue(p3.testColumn(0, 8, -2));
	}
		
}
