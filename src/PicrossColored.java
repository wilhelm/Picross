import javax.swing.BorderFactory;
import javax.swing.JPanel;

import java.awt.*;
import java.io.*;
import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PicrossColored extends JPanel {
    /*
     * definition of the dimensions of each cell
     */
	int cell_width;
    int cell_height;
    /*
     * how many cells do we have ?
     */
    int nb_columns;
    int nb_rows;
    int nb_clues_rows;
    int nb_clues_columns;
    /* 
     * what colors will we use ?
     */
    Color unknown;
    Color[] checked ;
    Color unchecked;
    /*
     * what clues do we have ?
     */
    LinkedList<LinkedList<int[]>> clues_columns;
    LinkedList<LinkedList<int[]>> clues_rows;
    LinkedList<int[]>[] possibilitiesRows;
    LinkedList<int[]>[] possibilitiesColumns;
    /*
     * values of the grid
     */
    int[][] a;//a[column][row]
    Cell[][] c;
    
    /**
     * (non-Javadoc)
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    public void paintComponent(Graphics g){
    	super.paintComponent(g);
    }
    
    /**
     * compute how many rows on top, and columns on left, we have to add for the clues
     * @param l : list of the clues
     * @return
     */
    public int maxclues(LinkedList<LinkedList<int[]>> l) {
    	int m = 0;
    	for(LinkedList<int[]> l1:l){
    		m = Math.max(m, l1.size());
    	}
    	return m;
    }
    
    public static Color parse(String input) {
        Pattern c = Pattern.compile("rgb *\\( *([0-9]+), *([0-9]+), *([0-9]+) *\\)");
        Matcher m = c.matcher(input);

        if (m.matches()) {
            return new Color(Integer.valueOf(m.group(1)),  // r
                             Integer.valueOf(m.group(2)),  // g
                             Integer.valueOf(m.group(3))); // b 
        }
        return null;  
    }
    
    /**
     * deal with the input file
     * affect nb_rows, nb_columns, clues_rows, clues_columns
     * @param file : the name of the file were with the clues for the picross
     */
    public void fileTreatment(String file) {
		try{
			System.out.println("file : "+file);
			/* read in the specified file */
			InputStream ips=new FileInputStream(file); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String ligne;
			/* save the read data in the picross */
			this.nb_rows = Integer.parseInt(br.readLine());//first line = number of cells in height
			this.nb_columns = Integer.parseInt(br.readLine());//second line = number of cells in width
			
			/* deal with the colors' list */
			ligne = br.readLine();
			ligne = ligne.substring(1, ligne.length()-1);
			String[] c = ligne.split(";");
			this.checked = new Color[c.length];
			for(int k = 0 ; k<c.length;k++) {
				this.checked[k] = parse(c[k]);
			}
			
			this.clues_rows = new LinkedList<LinkedList<int[]>>();
			this.clues_columns = new LinkedList<LinkedList<int[]>>();
			
			//computes clues_rows
			for(int k = 0 ; k<nb_rows; k++){//for every row
				LinkedList<int[]> l = new LinkedList<int[]>();
				ligne = br.readLine();//get the associated clues' line
				ligne = ligne.substring(1, ligne.length()-1);//get rid of the []
				if(ligne.length() > 0) {
					String[] a = ligne.split(";");//dissociate the clues
					for(int i = 0; i<a.length;i++){
						String[] b = a[i].substring(1, a[i].length()-1).split(",");
						int[] n = new int[2];
						n[0] = Integer.parseInt(b[0]);
						n[1] = Integer.parseInt(b[1]);
						l.add(n);//fill the List
					}
				}
				this.clues_rows.add(l);
			}
			//computes clues_columns, same as previously
			for(int k = 0 ; k<nb_columns; k++){
				LinkedList<int[]> l = new LinkedList<int[]>();
				ligne = br.readLine();
				ligne = ligne.replace("[", "");
				ligne = ligne.replace("]","");
				if(ligne.length() > 0) {
					String[] a = ligne.split(";");//dissociate the clues
					for(int i = 0; i<a.length;i++){
						String[] b = a[i].substring(1, a[i].length()-1).split(",");
						int[] n = new int[2];
						n[0] = Integer.parseInt(b[0]);
						n[1] = Integer.parseInt(b[1]);
						l.add(n);//fill the List
					}
				}
				this.clues_columns.add(l);
			}
			br.close(); //end of reading file
			System.out.println("file read");
		}
		catch (Exception e){
			System.out.println(e.toString());
		}
    	
    }
    
    /**
     * create a shallow copy of a list of list of int
     * @param l
     * @return l1, shallow copy of l
     */
    public LinkedList<LinkedList<int[]>> copyList(LinkedList<LinkedList<int[]>> l) {
    	LinkedList<LinkedList<int[]>> l1 = new LinkedList<LinkedList<int[]>>();
        for(LinkedList<int[]> s:l){
        	LinkedList<int[]> l2 = (LinkedList<int[]>) s.clone();
        	l1.add(l2);
        }
        return l1;
    }
    
    /**
     * generates the graphic aspect of our picross
     */
    public void generateGraphics() {
    	/* copy the clues in order to avoid destroying them */
    	LinkedList<LinkedList<int[]>> clues_column = copyList(clues_columns);
    	LinkedList<LinkedList<int[]>> clues_row = copyList(clues_rows);
		/* we use a grid layout */
        setLayout(new GridBagLayout());
    	/* object to locate components */
        GridBagConstraints gbc = new GridBagConstraints();
        /* no cell is merged */
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        int value;//this will be the eventual value of the clue
        
        /* create the display for the columns clues */
        for(int k = nb_clues_columns-1 ; k >= 0 ; k--){
            for(int i = 0 ; i < nb_columns ; i++){//for each column
                Cell c;
                if(!clues_column.get(i).isEmpty()){
					int[] n = clues_column.get(i).removeLast();
					value = n[0];
	                c = new Cell(cell_width, cell_height, checked[n[1]], value);
				}
				else {
	                c = new Cell(cell_width, cell_height, unchecked, null);
				}
                //set the cell at (nb_clues_rows+column number, nb_clues_columns-number_clue)
                gbc.gridx = nb_clues_rows + i;
                gbc.gridy = k;
                if(i == nb_columns){//go to the next line
                	gbc.gridwidth = GridBagConstraints.REMAINDER;
                }
                c.setBorder(BorderFactory.createLineBorder(Color.black, 2));
                add(c,gbc);
            }
        }
        
        /* create the display for the rows clues */
        for(int i = 0 ; i < nb_rows ; i++){
            for(int k = nb_clues_rows-1 ; k >= 0 ; k--){
                Cell c;
				if(!clues_row.get(i).isEmpty()){
					int[] n = clues_row.get(i).removeLast();
					value = n[0];
	                c = new Cell(cell_width, cell_height, checked[n[1]], value);
				}
				else {
	                c = new Cell(cell_width, cell_height, unchecked, null);
				}
                gbc.gridx = k;
                gbc.gridy = nb_clues_columns+i;
                c.setBorder(BorderFactory.createLineBorder(Color.black, 2));
                add(c,gbc);
            }
        }
        /* sets the inside of the picross */
        for(int k = 0 ; k < nb_columns ; k++){
            for(int i = 0 ; i < nb_rows ; i++){
                Cell c = new Cell(cell_width, cell_height, unknown, null);
                gbc.gridx = nb_clues_rows+k;
                gbc.gridy = nb_clues_columns+i;
                if(k == nb_columns-1){
                	gbc.gridwidth = GridBagConstraints.REMAINDER;
                }
                this.c[k][i] = c;
                a[k][i] = -1;
                add(c,gbc);
            }
        }
        this.setPreferredSize(new Dimension(800, 800));
        this.setMinimumSize(new Dimension(800, 800));
    }
    
    /**
     * build a picross from a file of clues
     * @param file : the name of the file with all the indications to build the picross
     */
    public PicrossColored(String file) {
		/* Instantiate the constants in the picross */
    	fileTreatment(file);
    	this.cell_width = 50;
	    this.cell_height = 50;
	    this.unknown = Color.GRAY;
	    this.unchecked = Color.WHITE;
	    this.a = new int[nb_columns][nb_rows];
	    this.c = new Cell[nb_columns][nb_rows];
	    /* number of additional cells for the clues */
	    this.nb_clues_columns = maxclues(clues_columns); 
	    this.nb_clues_rows = maxclues(clues_rows);
	    this.possibilitiesRows = new LinkedList[this.nb_rows];
	    this.possibilitiesColumns = new LinkedList[this.nb_columns];
	    this.setSize(new Dimension(600, 600));
	    generateGraphics();
	    /* compute all possibilities for rows and columns */
	    for(int k = 0; k<this.nb_rows; k++) {
	    	this.possibilitiesRows[k] = transformPossibilities(
	    			computePossibilities(k, (LinkedList<int[]>) this.clues_rows.get(k).clone(), 0, true, -1),
	    			true, k);
	    }
	    for(int k = 0; k<this.nb_columns; k++) {
	    	this.possibilitiesColumns[k] = transformPossibilities(
	    			computePossibilities(k, (LinkedList<int[]>) this.clues_columns.get(k).clone(), 0, false, -1), 
	    			false, k);
	    }
	    System.out.println("Picross instancié");
		//this.setPreferredSize(new Dimension(cell_width*(nb_columns + nb_clues_rows),cell_height*(nb_rows)));
    }
    
    /**
     * tests if the disposition on the column is convenient with the column's clues until row r
     * @param c : int, index of the tested column
     * @param r : int, index of the first unconsidered row
     * @param p = int, index of the previous color
     * @return
     */
    public boolean testColumn(int c, int r, int p) {
    	int i = 0;
    	int j = 0;
    	LinkedList<int[]> l = (LinkedList<int[]>) clues_columns.get(c).clone();//clone the clues for not destroying them
    	while(i<r){//while we still match the given area
    		if(a[c][i] == -2) {//if we have a blank cell
    			if(p == -2) {//if we are in a white block
    				//go on
    			}
    			else {//it's the end of the previous block
    				p = -2;
    				int k = l.getFirst()[0];
    				if(j == l.pop()[0]) {//we have enough colored cells
    					//go on
    					j = 0;
    				}
    				else {//we have less cells than expected
    					return false;
    				}
    			}
    		}
    		else {//the cell is colored
    			if(p == -2) {//if we begin a block
    				p = a[c][i];
    				if((l.isEmpty())||(p != l.getFirst()[1])) {//if we don't have the right color
    					return false;
    				}
    				j = 1;//go on
    			}
	    		else {//we were in a colored block
	    			if(a[c][i] == p) {//same color as previously
	    				if(j == l.getFirst()[0]) {//we already have enough cells
	    					return false;
	    				}
	    				else {
		    				j++;//go on
	    				}
	    			}
	    			else {//color is different
	    				p = a[c][i];
	    				if(j < l.pop()[0]) {//we don't have enough cells
	    					return false;
	    				}
	    				if((l.isEmpty()) || (p != l.getFirst()[1])) {//if we don't have the right color
	    					return false;
	    				}
	    				j = 1;//go on
	    			}
	    		}
    		}
    		i++;
    	}
    	return r<nb_rows || l.isEmpty() || (j == l.pop()[0] && l.isEmpty() && r == nb_rows);
    }

    /**
     * fill the row with unchecked cells
     * @param r index of row
     * @param i index of beginning of unknown
     */
    public void endRow(int r, int i) {
    	for(int k=i;k<nb_columns;k++) {
    		a[k][r] = -2;
    		c[k][r].changeColor(unchecked);
    	}
    }
    
    /**
     * tests if every column is convenient with the clues until column c
     * @param c : int, index of the first unconsidered column
     * @param r : int, index of the first unconsidered row
     * @return if the disposition suits to the clues
     */
    public boolean testColumns(int c, int r) {
    	boolean b1 = true;
    	int i = 0;
    	while(b1 && i<c) {
    		b1 &= testColumn(i,r, -2);
    		i++;
    	}
    	return b1;
    }
    
    /**
     * compute all the possibilities for an item (row or column) of our picross
     * @param r : int, index of our item
     * @param clues : list of the considered clues for the item
     * @param j : index of first placement of the first clue
     * @param b : if the item is a row or not
     * @param p : index of the color of the previous clue
     * @return the list of possibilities in following format : array containing the indexes of the first cell of each block
     */
    public LinkedList<int[]> computePossibilities(int r, LinkedList<int[]> clues, int j, boolean b, int p) {
    	//for each possibility to place the first block following the clues
    	int nb;
    	if(b) {
    		nb = this.nb_columns;
    	}
    	else {
    		nb = this.nb_rows;
    	}
    	if(clues.isEmpty())   {//if we don't have a clue at all
    		LinkedList<int[]> l = new LinkedList<int[]>();
    		int[] t = new int[0];
    		l.add(t);
    		return l;
    	}
    	else {
    		LinkedList<int[]> l = new LinkedList<int[]>();
    		int[] n = clues.pop();
    		// if we have two blocks of the same color in a range, then they have to be trained, if not, they can touch each other
    		if(n[1] == p) {
    			j++;
    		}
    		for(int k = j; k<nb; k++) {//for each possibility of begin cell
    			LinkedList<int[]> l1;
    	    	if(clues.isEmpty()) {//if this clue was the last
    	    		if(k+n[0]<=nb){
    	    			int[] t = {k};
    	    			l.add(t);
    	    		}
    	    	}
    	    	else {
        			l1 = computePossibilities(r, (LinkedList<int[]>) clues.clone(), k+n[0], b, n[1]);
        			if(!l1.isEmpty()) {
            			for(int[] t: l1){
            				int[] t1 = new int[t.length+1];
            				t1[0] = k;
            				for(int j1 = 1;j1<=t.length; j1++){
            					t1[j1] = t[j1-1];
            				}
            				l.add(t1);
            			}
        			}
    	    	}
    	    	//if(k==0)System.out.println(r+" possibilities computed");//allows us to check where the programm ran out of memory (just in case)
    		}
    		return l;
    	}
	}

    
    /*public void computePossibilitiesOptimised(int r, LinkedList<int[]> clues, boolean b) {
    	int nb;
    	if(b) {
    		nb = this.nb_columns;
    	}
    	else {
    		nb = this.nb_rows;
    	}
    	/* l is the list of the last possible index of each block */
    	/*LinkedList<int[]> l = new LinkedList<int[]>();
    	int i = nb-1;
    	for(int k=clues.size()-1; k>=0; k--) {
    		i -= clues.get(k);
    		if(k != clues.size()-1) {
    			i--;
    		}
    		l.addLast(i);
    	}
    	/*
    	 * compute possibilities with that reduced intervals
    	 */
    //}
    
    /**
     * transform the list of possibilities computed recursively (with only the beginning of each block) into 
     * the list of possibilities usable in the combination and exclusion part (with the value of each cell)
     * @param l = list of computed possibilities
     * @param b = row (true) or column
     * @param i = index of our item
     * @return
     */
    public LinkedList<int[]> transformPossibilities(LinkedList<int[]> l, boolean b, int i) {
		LinkedList<int[]> l1 = new LinkedList<int[]>();
		LinkedList<LinkedList<int[]>> clues;
		int nb;
    	if(b){//we are dealing with rows
    		nb = this.nb_columns;
    		clues = this.clues_rows;
    	}
    	else {//we are dealing with columns
    		nb = this.nb_rows;
    		clues = this.clues_columns;
    	}
    	while (!l.isEmpty()) {
        	int[] t = new int[nb];
    		int[] t1 = l.pop();
    		for(int k=0; k<nb; k++) {
    			t[k] = -2;
    		}
    		for(int k=0; k<t1.length;k++) {//for each clue
    			int[] c = clues.get(i).get(k);
    			int n = c[0]+t1[k];
    			for(int j=t1[k];j<n; j++) {//for each element of the block
    				t[j] = c[1];
    			}
    		}
    		l1.add(t);
    	}
    	return l1;
    }
    
   /**
     * places a block of length l on the row r after index i, returns index of last occupied cell
     * @param l : length of the block
     * @param i : index of the the first cell of the block
     * @return if the block can be placed
     */
    public boolean placeBlock(int l, int i){
    	return i+l<= nb_columns;
    }
    
    /**
     * place a computed "int" possibility on a row
     * @param r : index of the row
     * @param po : considered possibility
     * @param clues : list of the clues for the row
     */
    /*public void placeIntPossibilityRow(int r, int[] po, LinkedList<int> clues) {
    	endRow(r, 0);
    	for(int k = 0; k<po.length; k++) {//for each block in that line
    		for(int i = 0; i<clues.get(k);i++){
           		a[po[k]+i][r] = true;
           		c[po[k]+i][r].changeColor(checked);
    		}
    	}
    }*/
    
    /**
     * place a transformed (boolean) possibility on a row
     * @param r : index of the row
     * @param po : considered possibility
     */
    public void placePossibilityRow(int r, int[] po) {
    	for(int k = 0; k<this.nb_columns; k++) {//for each cell in that line
          	a[k][r] = po[k];
          	if(po[k]==-2){
          		c[k][r].changeColor(unchecked);
          	}
          	else {
          		c[k][r].changeColor(checked[po[k]]);
          	}
    	}
    }
    
    /**
     * place an allowed possibility on the row
     * @param r : index of the row
     * @param po : list of possibilities for that row
     * @return if such a possibility exists
     */
    public boolean placeAllowedPossibilityRow(int r, LinkedList<int[]> po) {
    	if(po.isEmpty()){
    		return false;
    	}
    	else {
        	int[] p = po.pop();
        	placePossibilityRow(r, p);
        	return testColumns(nb_columns, r+1) || placeAllowedPossibilityRow(r, po);
    	}
    }

    /**
     * solves the backtracking for a row
     * @param r : index of the first unconsidered row
     * @param as : current state of the possibilities
     * @return if there is a solution to the problem
     */
    public boolean solveBacktrackingRows(int r, LinkedList<int[]>[] as) {
    	if(r == this.nb_rows) {
    		return true;
    	}
    	else {
    		if(placeAllowedPossibilityRow(r, as[r])){
    			//if we are able to place one of the possibilities
    			return solveBacktrackingRows(r+1, as);
    		}
    		else {
    			as[r] = (LinkedList<int[]>) this.possibilitiesRows[r].clone();
    			for(int k=0; k<this.nb_columns;k++) {
    				a[k][r] = -1;
    				c[k][r].changeColor(unknown);
    			}
    			return solveBacktrackingRows(r-1, as);
    		}
    	}
    }
    
    /**
     * solve the picross using backtracking
     */
    public void solveBacktracking() {
    	LinkedList<int[]>[] as = new LinkedList[this.nb_rows];
    	for(int k=0; k<this.nb_rows; k++) {
    		as[k] = (LinkedList<int[]>) possibilitiesRows[k].clone();
    	}
    	System.out.println(this.solveBacktrackingRows(0, as));
    }
    
    /**
     * compare an array of Boolean to an array of boolean, and update the first one
     * @param a : state of a row
     * @param p : possibility
     * @return if there is a modification brought by this possibility
     * @author guillaume, Vincent
     */
    public boolean compareItem(int[] a, int[] p) {
    	boolean b = false;
    	for(int k=0; k<a.length; k++) {
    		if(a[k] != -1) {
    			if(a[k] != p[k]) {
    				a[k] = -1;
    				b = true;
    			}
    		}
    	}
    	return b;
    }
    
    /**
     * 
     * @param possibilities
     * @param nb
     * @param b
     * @author guillaume
     */
    public void initializeItem(LinkedList<int[]>[] possibilities, int nb, boolean b) {
    	if(b) {
    		possibilities = this.possibilitiesRows;
    		nb = nb_columns;
    	}
    	else {
    		possibilities = this.possibilitiesColumns;
    		nb = nb_rows;
    	}
    }
    
    /**
     * modify a cell according to the type of considered item (row or column)
     * @param b : if the item is a row
     * @param v : the value to give to the cell
     * @param i : first index
     * @param j : second index
     * @param c : Color to give to the cell
     * @author guillaume, Vincent
     */
    public void modifyFollowingItem(boolean b, int v, int i, int j, Color c) {
		if(b) {
			this.a[j][i] = v;
			this.c[j][i].changeColor(c);
		}
		else {
			this.a[i][j] = v;
			this.c[i][j].changeColor(c);
		}
    }
    
    /**
     * computes which cells we can tell they are black, and update the state and color of the picross, for an item (row or column)
     * @param b : if the item is a row
     * @param i : index of the item
     * @param possibilities : list of possibilities for the item
     * @param nb : amount of opposite items
     * @author guillaume, Vincent
     */
    public void interpolatePossibilities(boolean b, int i, LinkedList<int[]> possibilities, int nb) {
		int[] u = new int[nb];
		/* u will be the first possibility */
		for(int j = 0; j<nb;j++) {
			u[j] = possibilities.getFirst()[j];
		}
		/* for each possibility, we compare item to the to-date state of the picross item */
		for(int[] v:possibilities) {
			this.compareItem(u, v);
    	}
		/* update a and c */
    	for(int j = 0; j<nb;j++){
    		if(u[j] != -1) {
    			if(u[j] == -2) {
    				this.modifyFollowingItem(b, u[j], i, j, this.unchecked);
    			}
    			else {
    				this.modifyFollowingItem(b, u[j], i, j, this.checked[u[j]]);
    			}
    		}
    	}
    }
    
    /**
     * launched the combination algorithm on all items (rows or columns)
     * @param b : if the items are rows 
     * @author Vincent
     */
	public void interpolation(boolean b) {
    	LinkedList<int[]>[] possibilities;
    	int nb = 0;
    	if(b) {
    		possibilities = this.possibilitiesRows;
    		nb = nb_columns;
    	}
    	else {
    		possibilities = this.possibilitiesColumns;
    		nb = nb_rows;
    	}
    	for(int i=0; i<possibilities.length; i++) {
    		/* apply the interpolation algorithm on each row or column */
    		this.interpolatePossibilities(b, i, possibilities[i], nb);
    	}
    }
    
    /**
     * eliminate possibilities that doesn't match the current state of the items
     * @param b : if the items are rows
     * @author Vincent
     */
    public boolean elimination(boolean b){
    	boolean b1 = false;
    	LinkedList<int[]>[] possibilities = null;
    	int nb = 0;
    	if(b) {
    		possibilities = this.possibilitiesRows;
    		nb = nb_columns;
    	}
    	else {
    		possibilities = this.possibilitiesColumns;
    		nb = nb_rows;
    	}
    	for(int i=0; i<possibilities.length; i++) {
    		LinkedList<int[]> l = possibilities[i];
    		LinkedList<int[]> l2 = new LinkedList<int[]>();
    		for(int[] v:l) {//for each possibility
    			int c = 0;
    			for(int j = 0; j<nb; j++) {
    				int u;
    				if(b) {
    					u = a[j][i];
    				}
    				else {
    					u =  a[i][j];
    				}
    				if(u != -1) {//if the cell is already determined
    					if (v[j] != u) {//if there is a difference between the possibility and the state of the picross
    						c = c+1;
    						break;
    					}
    				}
    			}
    			if(c == 0) {//there is no difference, the possibility is still a possibility, keep consider it
					l2.add(v);
    			}
    			else {
    				b1 = true;
    			}
    		}
    		possibilities[i] = l2;//we keep only the possibilities that match the current state of the grid
    	}
    	return b1;
    }
    
    /**
     * solve the picross using combination and elimination
     * @author Vincent
     */
    public void solveOpt(){
    	boolean b = true;
    	while(b){
        	this.interpolation(true);
        	this.interpolation(false);
    		b = this.elimination(true) || this.elimination(false);
    	}
    	this.solveBacktracking();
    }
}
