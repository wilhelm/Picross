import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.LayoutManager;

/**
 * cell of the picross
 * @author guillaume
 *
 */
public class Cell extends JPanel {
	
	Color background;
    Integer value;
    int width;
    int height;
    
    /**
     * creates a cell for the picross
     * @param w : int, width of the cell
     * @param h : int, height of the cell
     * @param c : Color, background color for the cell
     * @param value : if the cell is a clue, the number it shall display
     */
    public Cell(int w, int h, Color c, Integer value) {
        this.background = c;
        this.width = w;
        this.height = h;
        if(value != null) {//avoid NullPointerException
            this.value = value;
        }
        this.setPreferredSize(new Dimension(w,h));//we want the cell to have the announced size
        this.setMinimumSize(new Dimension(20,20));//the cell has to be at least 10*10
        this.setBackground(this.background);//color the cell
        this.setBorder(BorderFactory.createLineBorder(Color.black));//add a border to the cell
    }
    
    /**
     * (non-Javadoc)
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    public void paintComponent(Graphics g){
    	super.paintComponent(g);
    	//display the value of the clue
        if(value != null) {
            this.setLayout(new BorderLayout());
            JLabel n = new JLabel(""+value, JLabel.CENTER);
            n.setForeground(Color.BLACK);
            this.add(n, BorderLayout.CENTER);
            //g.drawString(""+value, 12, 12);
        }
        this.setVisible(true);
    }
    
    /**
     * change the color of the cell
     * @param c : Color, the wanted color for the background of this cell
     */
    public void changeColor(Color c) {
    	this.background = c;
        setBackground(c);
    }
}
