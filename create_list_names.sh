#!/bin/bash
#aims at creating a list of all picrosses in the folders C and BW
#how to use it : 
## type create_list_names name_of_your_folder name_of_the_folder_in_lower_case
#you can now launch the Test class (Java part of this project)

creation() {
    folder=$1
    destination_file=$2
    for fichier in `ls -Sr $folder`
	do
	    echo ${fichier%%.*} >> $destination_file
	done
}
    
creation $*
